
#include <stdbool.h>
#include <aos/aos.h>
#include <yoc/yoc.h>
#include <yoc/partition.h>

#include "app_init.h"
#include "board.h"

#define TAG "INIT"

#ifndef CONSOLE_UART_IDX
#define CONSOLE_UART_IDX 0
#endif

void board_yoc_init()
{
    board_init();
    mtd_init();
    console_init(CONSOLE_UART_IDX, 115200, 128);
    ulog_init();
    aos_set_log_level(AOS_LL_DEBUG);

    int ret = partition_init();
    if (ret <= 0) {
        LOGE(TAG, "partition init failed");
    } else {
        LOGI(TAG, "find %d partitions", ret);
    }

    LOGI(TAG, "Build:%s,%s",__DATE__, __TIME__);
#if FLASH_BOOT_ENABLE
    first_time_bootup();
#endif
}
