#include <aos/kernel.h>
#include <ulog/ulog.h>
#define SMXUSBH
#include "smxbase.h"
#include "smxusbh.h"
#include "smxfs.h"
#include <yoc/partition.h>

#define TAG "usb_ota"

// #define USB_USE_EVENT

#define PARTITION_SECTOR_NUM (10)

static int usb_download(void *priv)
{
    char *fileName = "a:\\fota.bin";
    FILEHANDLE pFile = NULL;
    int file_len, remain_len, read_len, offset, buf_len;


    if(SFS_DEVICE_MOUNTED != sfs_devstatus(0)) {
        LOGD(TAG, "usb device not mounted \n");
        return -1;
    }

    if (NULL == (pFile = sfs_fopen(fileName, "rb"))) {
        LOGD(TAG, "File read open failed \n");
        return -1;
    }

    partition_t part_hdl = partition_open("misc");
    if (part_hdl < 0) {
        LOGD(TAG, "misc partition open failed \n");
        return -1;
    }

    partition_info_t *info = partition_info_get(part_hdl);
    buf_len = info->sector_size * PARTITION_SECTOR_NUM;
    char *buf = aos_malloc_check(buf_len);

    if (sfs_fseek(pFile, 0, SFS_SEEK_SET)) {
        LOGD(TAG, "File seek error \n");
        return -1;
    }

    file_len = sfs_filelength(fileName);
    LOGD(TAG, "file len: %d \n", file_len);

    read_len    = buf_len >= file_len ? file_len : buf_len;
    remain_len  = file_len;
    offset = 2 * info->sector_size;
    LOGD(TAG, "read begin : \n");

    while(remain_len)
    {
        size_t ret = sfs_fread(buf, read_len, 1, pFile);
        if (ret < 0) {
            LOGD(TAG, "u misc read err \n");
            break;
        }
        LOGD(TAG, "usb to flash %d%%", offset *100/file_len);
        partition_erase(part_hdl, offset, (read_len+info->sector_size-1)/info->sector_size);
        partition_write(part_hdl, offset, buf, read_len);
        sfs_fseek(pFile, 0, SFS_SEEK_CUR);

        remain_len -= read_len;
        offset += read_len;
        read_len = buf_len >= remain_len ? remain_len : buf_len;
    }

    LOGD(TAG, "usb to flash over ! \n");
    aos_free(buf);
    partition_close(part_hdl);
    sfs_fclose(pFile);
    aos_reboot();

    return 0;
}

#define SMXUSBH

void usb_ota_task(void *priv)
{
    while(1) {
        su_CheckRoothubStatus();
        if(SFS_DEVICE_MOUNTED == sfs_devstatus(0)) {
            usb_download(NULL);
        }

        aos_msleep(1000);
    }
}

static void setUsbClkEnble()
{
    volatile uint32_t *base;
    uint32_t temp;

    /* config_usb_clk_en */
    base = (uint32_t *)(CSKY_CLKGEN_BASE + USB_CLK_CFG);
    temp = *base;
    /* clkgen_usb_phy_rclk_gate_en */
    temp |= 0x1 << 5;
    /* clkgen_usb_hclk_gate_en */
    temp |= 0x1 << 6;
    temp |= 0x0 << 3;
    temp |= 0x1 << 3;
    *base = temp;

    //usb_phy_clock_de_reset
    base = (uint32_t *)(CSKY_RSTGEN_BASE + 0x10);
    *base |= 0x1 << 12;
}

int USBHDemoStart(void)
{
    int result = -1;
#if defined(SB_CPU_MCF5445x)
    if(MCF_USB_OTG_OTGSC&MCF_USB_OTG_OTGSC_ID)
        return -1;
#endif
    // USBHPutString("smxUSBH Demo Started.");
    if(su_Initialize())
    {
#if DEMO_AUDIO
#if ONLY_TEST_PLAYBACK
        g_pPlaybackData = (u8 *)pStereoSoundData;
#endif
#endif
#if DEMO_MOUSE
        su_MouseSetCallback(USBMouseHandler);
#endif
#if DEMO_KBD
        su_KbdSetCallback(USBKBDHandler);
#endif
#if DEMO_HID
        su_HIDSetCallback(USBHIDHandler);
#endif
#if 1//defined(SMXFS)
    if(sfs_init() == SB_FAIL) {
        // USBHPutString("sfs_init() failed.");
        return -1;
    }

    sfs_devreg(sfs_GetUSB0Interface(), 0);
#endif
#if DEMO_FTDI232
        g_pFTDI232Buf = (u8 *)malloc(1024);
#endif
#if DEMO_PL2303
        g_pPL2303Buf = (u8 *)malloc(SU_PL2303_MTU);
        su_PL2303LineStateChangeNotify(PL2303Handle);
#endif
#if DEMO_SERIAL
        su_SerialLineStateChangeNotify(SerialHandle);
#endif

#if DEMO_VIDEO
#if VIDEO_DISPLAY
        g_pFrameBuffer = (u8 *)malloc(VIDEO_CAPTURE_WIDTH*VIDEO_CAPTURE_HEIGHT*2);
        if(g_pFrameBuffer)
        {
            int i;
            InitializeLcd((char *)g_pFrameBuffer, VIDEO_CAPTURE_WIDTH, VIDEO_CAPTURE_HEIGHT);
            memset(g_pFrameBuffer, 0xFF, VIDEO_CAPTURE_WIDTH*VIDEO_CAPTURE_HEIGHT*2);
            for(i = 0; i < VIDEO_CAPTURE_WIDTH*VIDEO_CAPTURE_HEIGHT*2;)
            {
                int R, G, B;
                R = 255;
                G = 0;
                B = 0;
                g_pFrameBuffer[i++]  = (u8) (((G & 0x3c) << 3) | (B >> 3));
                g_pFrameBuffer[i++]  = (u8) ((R & 0xf8) | (G >> 5));;
            }
        }
        else
        {
            return -1;
        }
#endif
#endif
        result = 0;
    }
    else
    {
        // USBHPutString("su_Initialize() failed.");
    }

    return result;
}

#ifdef USB_USE_EVENT
void usb_event(uint iID, BOOLEAN bInserted)
{
    aos_task_t task;
    aos_task_new_ext(&task, "usb_ota", usb_download, NULL, 2048, AOS_DEFAULT_APP_PRI);
}
#endif

void app_usb_ota_init(void)
{
    setUsbClkEnble();
    USBHDemoStart();
#ifdef USB_USE_EVENT
    su_MSRegDevEvtCallback(usb_event);
#else
    aos_task_t task;
    aos_task_new_ext(&task, "usb_ota", usb_ota_task, NULL, 2048, 25);
#endif
}