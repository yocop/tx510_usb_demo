#include <stdio.h>
#include <string.h>
#include <aos/debug.h>

#include "iic_function.h"
#include "sfh4775s.h"
#include "reg_deal.h"

#define TAG "led"

static i2c_dev_t i2c_ir_led;

static regval_list irled_start[] = {
#if defined(LED_STROBE)
    {0x01,0x21},  //LED1:speckle;LED2~LED4,ir
#else
    {0x01,0x0d},  //LED1:speckle;LED2~LED4,ir
#endif
    // {0x01 ,0x09}, //LED1:speckle;LED2~LED4,ir
    {0x02,0x01},
    {IR_CURRENT_REG, 0x3f}, //0x3f 500mA , 0x7f 1000mA
    {SPK_CURRENT_REG, 0x3f}, //0xcc 1600mA
    //{0x05 ,0x7f},
    //{0x07 ,0xcd},
    //{0x08 ,0x7f},
    //{0x09 ,0xcd},
    //{0x0a ,0x7f},
    {0x08,0x0f},  //0x9a
};

#if 0
static regval_list irled_open[] = {
#if defined(LED_STROBE)
    {0x01,0x21},
#else
    {0x01,0x0d},  //LED1:speckle;LED2~LED4,ir
#endif
};
#endif

static const regval_list irled_stop[] = {
#if defined(LED_STROBE)
    {0x01,0x20},
#else
    {0x01,0x00},
#endif
};

static const  regval_list speckle_start[] = {
#if defined(LED_STROBE)
    {0x01,0x22},
#else
    {0x01,0x0e},  //32
#endif
    {SPK_CURRENT_REG, 0x3f}, //0xcc 1600mA
    //{0x02 ,0xe1},
    //{0x03 ,0x7f},
    //{0x05 ,0x7f},
    //{0x0b ,0x1f},
};

static const regval_list speckle_start_outside[] = {
#if defined(LED_STROBE)
    {0x01,0x22},
#else
    {0x01,0x0e},  //32
#endif
    {SPK_CURRENT_REG, 0x7f}, //0x7f 1000mA
    //{0x02 ,0xe1},
    //{0x03 ,0x7f},
    //{0x05 ,0x7f},
    //{0x0b ,0x1f},
};

static const  regval_list speckle_torch_start[] = {
    {0x01,0x09},
    //{SPK_CURRENT_REG, 0x3f}, //0xcc 1600mA
    //{0x02 ,0xe1},
    //{0x03 ,0x7f},
    //{0x05 ,0x7f},
    //{0x0b ,0x1f},
};

static int32_t check_id_irled(i2c_dev_t *i2c)
{
    uint8_t data = 0;
    int32_t ret = iic_read(i2c, IRLED_REG_CHIPID, &data);
    //LOGD(TAG,"ir led id: %02x \n",data);
    if (ret < 0 || data != 0x30)
        return -1;

    return ret;
}

int ir_led_init(int i2c_num, uint32_t dev_addr)
{
    i2c_ir_led.universal  = 1;
    i2c_ir_led.port       = 11;
    i2c_ir_led.i2cNo      = i2c_num;

    i2c_ir_led.config.address_width = 0;
    i2c_ir_led.config.freq          = 400000;
    i2c_ir_led.config.mode          = 0;

    hal_i2c_init(&i2c_ir_led);

    i2c_ir_led.config.dev_addr = dev_addr;
    int ret = check_id_irled(&i2c_ir_led);
    if (ret < 0) {
        LOGD(TAG,"ir led check id fail\n");
        return ret;
    }
    return ret;
}

int ir_led_finalize()
{
    return 0;
}

int32_t led_check_enable(void)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, IRLED_REG_ENABLE, &data);
    if (ret < 0)
        return ret;

    return (data & 0x0c); //According to DS, all mode bits in enable register should be cleard after flash timeout
}

int32_t led_read_flag(uint32_t reg)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, reg, &data);
    //LOGD(TAG,"ir flag %02x \n",data);
    if (ret < 0)
        return ret;
    return data;
}

int ir_led_power_on(void)
{
    int32_t ret = 0;
    //ret = iic_write_array(pcsi_iic_3, (struct regval_list *)irled_start, ARRAY_SIZE(irled_start));
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)irled_start, ARRAY_SIZE(irled_start));
    if (ret < 0) {
        return ret;
    }
    //iic_read_array_r8_d8(&i2c_ir_led,(regval_list *)irled_start, ARRAY_SIZE(irled_start));
    return ret;
}

int ir_led_power_off(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led,  (regval_list *)irled_stop, ARRAY_SIZE(irled_stop));
    if (ret < 0) {
        LOGD(TAG,"ir led close failed \n");

    }
    return ret;
}

int speckle_power_on(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));
    if (ret < 0) {
        LOGD(TAG,"ir led close failed \n");
    }
    return ret;
}

int speckle_torch_power_on(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_torch_start, ARRAY_SIZE(speckle_torch_start));
    if (ret < 0) {
        LOGD(TAG,"ir led close failed \n");
    }
    return ret;
}

int speckle_power_on_outside(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start_outside, ARRAY_SIZE(speckle_start_outside));
    if (ret < 0) {
        LOGD(TAG,"ir led close failed \n");
    }
    return ret;
}


int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    uint32_t array_size = 0;

    array_size = ARRAY_SIZE(irled_start);
    config_reglist(irled_start, array_size, addr, data);

    return 0;
}


uint32_t led_lookup_reg(uint32_t addr)
{
    uint32_t array_size = 0;

    array_size = ARRAY_SIZE(irled_start);
    return lookup_reg(irled_start, array_size, addr);
}