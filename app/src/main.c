/*
 * Copyright (C) 2015-2017 Alibaba Group Holding Limited
 */

#include <app_config.h>

#include <stdlib.h>
#include <string.h>
#include <aos/aos.h>
#include "main.h"
#include "app_init.h"
#include "sfh4775s.h"

#define TAG "app"


/*************************************************
 * YoC入口函数
 *************************************************/

int main(void)
{
    board_yoc_init();
    LOGD(TAG, "%s\n", aos_get_app_version());

    app_usb_ota_init();

    LOGI(TAG, "usb upgrade start");

    ir_led_init(1, 99);

    while (1) {
        ir_led_power_on();
        aos_msleep(1500);
        ir_led_power_off();
        aos_msleep(1500);
    }

    return 0;
}
