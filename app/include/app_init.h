#ifndef __APP_INIT_H
#define __APP_INIT_H
#include <aos/aos.h>
#include "bootpara_deal.h"

void board_cli_init();
void board_yoc_init(void);

#endif
