# 概述

`usb_upgrade`是一个简单的u盘升级demo

# 编译

```bash
make clean; make
```

# 启动

## 使用gdb load功能调试

### 初始化ddr

```bash
csky-abiv2-elf-gdb TX216_EVB_Bootldr_QSPI_cpu400_aie360_ddr800.elf -x ck804_ddr_init.gdbinit
```
### 启动程序
```bash
csky-abiv2-elf-gdb yoc.elf 
```

## 使用烧写固件方法调试

## 烧写

```
make flash // 烧写prim分区
make flashall //烧写所有分区
```



## 使用方法

- 编译完成后，在demo目录下生成fota.bin，将该拷贝到u盘根目录下，
- 烧写完成后，将U盘插入设备，重新上电即可，串口输出如下log，表示设备正在将U盘中的升级固件更新到设备中

```
[   0.135]<I>app usb upgrade start
wTempData = 0xaa55wTempData = 0xaa55CheckDevice res = 0x0
[   1.400]<D>usb_ota file len: 201684 

[   1.403]<D>usb_ota read begin : 

[   1.411]<D>usb_ota usb to flash 4%
[   1.655]<D>usb_ota usb to flash 24%
[   1.896]<D>usb_ota usb to flash 44%
[   2.138]<D>usb_ota usb to flash 64%
[   2.381]<D>usb_ota usb to flash 85%
[   2.600]<D>usb_ota usb to flash over ! 
```

**注意：**

该demo不做版本管理，升级后需要将U盘拔下

